/////////////////// IMPORT TECHNO
import * as THREE from "./build/three.module.js";
import { FirstPersonControls } from "./jsm/controls/FirstPersonControls.js";
import { GLTFLoader } from './jsm/loaders/GLTFLoader.js';

/////////////////// IMPORT OBJETS ET TEXTURES

import Cube from "./objects/cube.js";
import TexturedCube from "./objects/texturedCube.js";
import directionalLight from "./objects/directionnalLight.js";

/////////////////// IMPORT SCRIPT
import { createButton, loadModel, render } from "./components/ARVR.js";

import * as AlgoGen from './genetics/geneticAlgorithm.js'

var camera, scene, renderer;

var current_object, controls

var scene = new THREE.Scene();
var city = new THREE.Object3D();
var townObjs = new THREE.Object3D()
var smoke = new THREE.Object3D();
var setTintNum = true;
var audioFiles;
var listenerAudio = new THREE.AudioListener();
var sound = [
  new THREE.Audio(listenerAudio),
  new THREE.Audio(listenerAudio),
  new THREE.Audio(listenerAudio),
]
var audioLoader = new  THREE.AudioLoader();

var rooms = 15

//var i = -1

var squareSize = 0.2
var squarePerSecond = 6
var roomLengthInSquares = 50
var groundSize = squareSize * rooms * roomLengthInSquares
var roomNum = 1

var vaisseau = new THREE.Object3D();

const clock = new THREE.Clock();
var glitchyObjects = []

import objects from './3d/objects.js'


const generations = getGenerations(rooms)

init();

function init() {
  const container = document.createElement("div");
  document.getElementById("container").appendChild(container);

  /////////////////// CREATE MAIN SCENE
  scene = new THREE.Scene();
  camera = new THREE.PerspectiveCamera(70,window.innerWidth / window.innerHeight,0.0001,200);
  camera.position.x = 0;
  camera.position.y = 0.1;
  camera.position.z = 0;
  camera.lookAt(new THREE.Vector3(0, 0.1, 0));

  /////////////////// RENDER
  renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
  renderer.setPixelRatio(window.devicePixelRatio);
  renderer.setSize(window.innerWidth, window.innerHeight);
  renderer.xr.enabled = true;
  container.appendChild(renderer.domElement);
  
  /////////////////// BUTTON ARVR
  createButton(current_object, scene, renderer)

  /////////////////// CAMERA DEPLACEMENT
  controls = new FirstPersonControls(camera, renderer.domElement);
  controls.movementSpeed = 100;
  controls.lookSpeed = 0.1;
  controls.noFly = true;

  /////////////////// ON RESIZE WINDOW CANVAS RESIZE
  window.addEventListener("resize", onWindowResize, false);

  /////////////////// CREATE OUR SCENES
  loadSound();
  initScene();
  
  for (let i = 1; i< rooms; i++) {
     setTimeout(playNewRoomSounds, i * (roomLengthInSquares / squarePerSecond) * 1000, i)
  }
	animate();
}

function playNewRoomSounds(roomNumber) {
  const genome = generations[roomNumber][0].genome.slice(0, sound.length)
  sound.forEach((s, i) => {
    if (s.isPlaying && genome.slice(i-1, i) === '0') { 
      console.log('stop sound 0'+i)
      s.stop()
    }
    else if (s.isPlaying === false && genome.slice(i-1, i) === '1') { 
      console.log('start sound 0'+i)
      s.play()
    }
  })
}

/////////////////// LOADING ALL MUSICS
function loadSound() {
  audioFiles = [
    'sound/07.mp3',
    'sound/01.mp3',
    'sound/04.mp3',
  ];

  const genome = generations[0][0].genome.slice(0, sound.length)
  audioFiles.forEach((audioFile, i) => {
    audioLoader.load(audioFile, function (buffer) {
      sound[i].setBuffer(buffer);
      sound[i].setLoop(true);

      if (genome.slice(i-1, i) === '1') { 
        console.log('start sound 0'+i)
        sound[i].play()
      }
    })
  });
  camera.add( listenerAudio );
}

function initScene(){
  var gridHelper = new THREE.GridHelper( groundSize, groundSize/squareSize, 0xFF0000, 0xffffff);
  scene.add( gridHelper );
  // const axesHelper = new THREE.AxesHelper( 5 );
  // scene.add( axesHelper );

  /////////////////// SET BACKGROUND
  var setColor = 0xF2F111; //yellow
  scene.background = new THREE.Color(setColor);

  createParticule()
  initFloor()
  createLight()
  createTown()
	scene.fog = new THREE.Fog(setColor, 5, 10);
}

/////////////////// IMPORT 3D MODEL
function importModel(until){
  const loader = new GLTFLoader();
  const max = (until)?until:objects.length
  for (let indexObject = 0; indexObject < max; indexObject++) {
    const object = objects[indexObject];
    loader.load( '3d/'+object.name+'/scene.gltf', function ( gltf ) {
      gltf.scene.scale.x = object.scale
      gltf.scene.scale.y = object.scale
      gltf.scene.scale.z = object.scale
      gltf.scene.position.z = -1
      gltf.scene.position.y = 0
      gltf.scene.position.x = object.distance
      gltf.scene.visible = false
      object.loaded = true
      object.scene = gltf.scene
      scene.add(gltf.scene)
    }, undefined, function ( error ) {
      console.error( error );
    } );
  }
  
 
}

/////////////////// LOADING 3D MODELS
function showModel(id, position) {
  const object = objects[id % 3]
  if (object.loaded) {
      object.scene.position.z = -1 -position.z
      object.scene.visible = true
 }

}

function getGenerationDataArray(generation) {
  const generationData = []
  for (let i=0; i<generation.length; i++) {
    const genomeData = AlgoGen.getDataFromGenome(generation[i])
    genomeData.genome = generation[i]
    generationData.push(genomeData)
  }
  return generationData
}

function getGenerations(numberOfGenerations) {
  let generation = AlgoGen.generateInitialPopulation();
	  console.log(`Gen [initial]`, 'global quality:', AlgoGen.getGenerationQuality(generation), 'sur', generation[0].length)
  const generations = [getGenerationDataArray(generation)]
	let newGen = generation
  for (let i=0; i< numberOfGenerations; i++) {
	  do {
		  //console.log(generation.map(gen => ([ gen, AlgoGen.fitness(gen) ])))
		  //console.log(AlgoGen.sortGenomes(generation).map(gen => ([ AlgoGen.fitness(gen) ])))
		  newGen = AlgoGen.generateNewGeneration(AlgoGen.sortGenomes(newGen))
	  } while (newGen.length < 2)
	  //console.log(`Gen [${i}]`, 'texure quality:', AlgoGen.getGenerationQuality(newGen, 9, 16), 'sur', 16 - 9)
	  console.log(`Gen [${i}]`, 'global quality:', AlgoGen.getGenerationQuality(newGen), 'sur', newGen[0].length)
    generations.push(getGenerationDataArray(newGen))
  }
	//console.log(generations)
  return generations
}

/////////////////// CREATE BUILDING
function createTown(){
  var segments = 2;
  var wmaterial = new THREE.MeshLambertMaterial({ color:0xFFFFFF, wireframe:true, transparent:true, opacity: 0.03, side:THREE.DoubleSide });

	const loadManager = new THREE.LoadingManager();
	const loader = new THREE.TextureLoader(loadManager);

	let buildingTextures = []

	for (let i=1; i<=38; i++) {
		//buildingTextures.push(new THREE.MeshBasicMaterial({
			//map: loader.load('./textures/buildings/'+i+'.jpeg'),
			////color: setTintColor(),
			//wireframe:false,
			//side:THREE.DoubleSide 
		//}))
		buildingTextures.push(loader.load('./textures/buildings/'+i+'.jpeg'))
	}

	loadManager.onLoad = () => {
		//const cube = new THREE.Mesh(geometry, material);

		const roomLengthInUnits = roomLengthInSquares * squareSize

		for (var i = 0; i<generations.length; i++) {
			const generation = generations[i]
			const roomStart = roomLengthInUnits * i
			townObjs.add(new THREE.Object3D())

			glitchyObjects.push([])

			for (var j = 0; j < generation.length; j++) {
				const building = new THREE.Object3D()
				const genomeData = generation[j]

				var material = new THREE.MeshPhongMaterial({ color: setTintColor(), wireframe:false, side:THREE.DoubleSide });

				var geometry = new THREE.CubeGeometry(1, 1, 1, segments, segments, segments);
				let texture = buildingTextures[genomeData.texture].clone()
				texture.needsUpdate = true
				texture.image.filter = 'blur(5px)'
				const textureRatio = texture.image.width / texture.image.height
				const buildingRatio = (0.9 * genomeData.scale.x) / (5 * genomeData.scale.y)

				const over = textureRatio - buildingRatio
				const percent = over / textureRatio

				texture.offset.x = 0.5
				texture.repeat.set( 1- percent, 1 )

				const textureMaterial = new THREE.MeshLambertMaterial({
					map: texture,
					color: 0x555555,
					wireframe:false,
					needsUpdate: true,
					opacity: (genomeData.opacity),
					transparent: true,
					side: THREE.DoubleSide 
				})
				if (genomeData.glitches === true) {
					glitchyObjects[i].push(textureMaterial)
				}
				//const newTexture = buildingTextures[genomeData.texture].clone()
				//buildingTextures[genomeData.texture].map.wrapS = THREE.RepeatWrapping
				//buildingTextures[genomeData.texture].map.wrapT = THREE.RepeatWrapping
				//buildingTextures[genomeData.texture].map.needsUpdate = true
				var cube = new THREE.Mesh(geometry, textureMaterial);
				var wire = new THREE.Mesh(geometry, wmaterial);
				var floor = new THREE.Mesh(geometry, material);
				var wfloor = new THREE.Mesh(geometry, wmaterial);

				cube.add(wfloor);
				cube.castShadow = true;
				cube.receiveShadow = true;
				cube.rotationValue = 0.1+Math.abs(mathRandom(8));

				floor.scale.y = 0.05;
				cube.scale.x = 0.9 * genomeData.scale.x + (AlgoGen.randomBit()?0.2:0)
				cube.scale.z = 0.9 * genomeData.scale.x
				cube.scale.y = 5 * genomeData.scale.y

				cube.position.y = cube.scale.y / 2
				cube.position.x = ((Math.round(Math.random()) === 0) ? -1 : 1) * genomeData.distance * squareSize * 2;
				const zPos =  i * roomLengthInUnits  + Math.random() * roomLengthInUnits
				cube.position.z = -zPos
				floor.position.set(cube.position.x, 0, cube.position.z)
				building.add(floor)
				building.add(cube)

				townObjs.children[i].add(building)
			}
			if (i === 0 || i === 1)
				townObjs.children[i].visible = true
			else
				townObjs.children[i].visible = false
		}
		city.add(townObjs);
	};
}

/////////////////// CHECK POSITION BUILDING
function checkPosition(cube, listCube){
  listCube.forEach(element => {
    if((cube.position.x > element.position.x-1 && cube.position.x < element.position.x+1) && (cube.position.z > element.position.z-1 && cube.position.z < element.position.z+1)){
      cube.position.x = Math.round(mathRandom(30));
      cube.position.z = Math.round(mathRandom(30));
      checkPosition(cube, listCube)
    }
  });
  if(cube.position.x > -1 && cube.position.x < 1){
    cube.position.x +=1;
  }
}

/////////////////// CREATE PARTICULE
function createParticule(){
  var gmaterial = new THREE.MeshToonMaterial({color:0xFFFF00, side:THREE.DoubleSide});
  var gparticular = new THREE.CircleGeometry(0.01, 5);
  var aparticular = 5;
  
  for (var h = 1; h<500; h++) {
    var particular = new THREE.Mesh(gparticular, gmaterial);
    particular.position.set(mathRandom(aparticular), mathRandom(aparticular),mathRandom(aparticular));
    particular.rotation.set(mathRandom(),mathRandom(),mathRandom());
    smoke.add(particular);
  };
  city.add(smoke);
}

/////////////////// CREATE MIRRORED FLOOR
function initFloor() {
   // CHANGE FLOOR COLOR
  var pmaterial = new THREE.MeshPhongMaterial({color:0x000000, side:THREE.DoubleSide, opacity:0.9, transparent:true});
  var pgeometry = new THREE.PlaneGeometry(groundSize, groundSize);
  var pelement = new THREE.Mesh(pgeometry, pmaterial);
  pelement.rotation.x = -90 * Math.PI / 180;
  pelement.position.y = -0.001;
  pelement.receiveShadow = true;
  city.add(pelement);
}

/////////////////// INIT LIGHTS
function createLight(){
  var ambientLight = new THREE.AmbientLight(0xFFFFFF, 4);
  var lightFront = new THREE.SpotLight(0xFFFFFF, 20, 10);
  var lightBack = new THREE.PointLight(0xFFFFFF, 0.5);

  lightFront.rotation.x = 45 * Math.PI / 180;
  lightFront.rotation.z = -45 * Math.PI / 180;
  lightFront.position.set(5, 5, 5);
  lightFront.castShadow = true;
  lightFront.shadow.mapSize.width = 6000;
  lightFront.shadow.mapSize.height = lightFront.shadow.mapSize.width;
  lightFront.penumbra = 0.1;
  lightBack.position.set(0,6,0);

  smoke.position.y = 2;

  scene.add(ambientLight);
	// DISABLED
  //city.add(lightFront);
  //scene.add(lightBack);
  scene.add(city);
  
}

/////////////////// MATH RANDOM FUNCTION
function mathRandom(num = 8) {
  var numValue = - Math.random() * num + Math.random() * num;
  return numValue;
};

/////////////////// SET COLOR
function setTintColor() {
  if (setTintNum) {
    setTintNum = false;
    var setColor = 0x000000;
  } else {
    setTintNum = true;
    var setColor = 0x000000;
  };
  return setColor;
};

/////////////////// ON RESIZE WINDOW
function onWindowResize() {
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();
  renderer.setSize(window.innerWidth, window.innerHeight);
}


var playing = true

export function play() {

	if (playing) {
		console.log('room Number:', roomNum)
		console.log('generation', generations[roomNum-1])
		playing = false
	} else
		playing = true
}

document.getElementById('play').addEventListener('click', play)

/////////////////// ANIMATION LOOP
function animate() {
  renderer.setAnimationLoop((timestamp, frame) => {
    render(timestamp, frame)
    renderer.render(scene, camera);
  });
	if (glitchyObjects[roomNum-1]) {
		for (let i=0; i<glitchyObjects[roomNum - 1].length; i++)  {
			if (glitchyObjects[roomNum-1][i].glitching != true) {
				if (Math.random() > 0.98) {
					glitchyObjects[roomNum-1][i].glitching = true
					glitchyObjects[roomNum-1][i].opacity = 0.5
					glitchyObjects[roomNum-1][i].map.needsUpdate = true
				}
			} else if (Math.random() > 0.2) {
				glitchyObjects[roomNum-1][i].opacity = 1
				glitchyObjects[roomNum-1][i].glitching = false
				glitchyObjects[roomNum-1][i].map.needsUpdate = true
			}
		}
	}
	if (playing) {
		if (scene.position.z <= (groundSize/2)-(squareSize*roomLengthInSquares)) {
			if (scene.position.z / (squareSize*roomLengthInSquares) >= roomNum) {
				if (roomNum > 2) {
					townObjs.children[roomNum-3].visible = false
				}
				if (roomNum+2 <= rooms) {
					townObjs.children[roomNum+1].visible = true
				}
				roomNum++
				console.log('room passed', roomNum)
				console.log('Building quality', generations[roomNum-1].map(el => el.texture).reduce((acc, curr) => acc+=curr, 0)/generations[roomNum-1].length, 'sur', 38)
			}
			scene.position.z += (squarePerSecond)/60 * squareSize
			smoke.position.z += (squarePerSecond)/60 * squareSize
		}
		else{
			scene.position.z =0;
		}
	}

  smoke.rotation.y += 0.01;
  smoke.rotation.x += 0.01;
 
  requestAnimationFrame(animate);
  controls.update( clock.getDelta() );
}
