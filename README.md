# Cyberpunk Style road

Ce projet a pour but de s’inspirer une expérience immersive de l’univers du jeu-vidéo Cyberpunk 2077.

## Un projet d'intelligence artificielle de BOYER Sophie - LIENART Nicolas - VALLET Gaëlle

## Instructions

- Cloner ce dépot GitLab
- Installer via npm l'émulateur de serveur local `npm install --global live-server`
- Démarrer le serveur `live-server .`
- Profiter de l'expérience à l'adresse indiquée dans le terminal

- *Optionel :* pour utiliser le simulateur de VR sans casque utiliser l'extenstion pour navigateur `WebXR API Emulator` 
