export default [
	{
		"name": "car",
		"scale": "0.1",
		"distance": 1
	},
	{
		"name": "mira",
		"scale": "0.001",
		"distance": 2
	},
	{
		"name": "model",
		"scale": "0.1",
		"distance": -0.5
	},
	{
		"name": "robot",
		"scale": "0.15"
	},
	{
		"name": "vaisseau",
		"scale": "0.1"
	},
	{
		"name": "good object 6",
		"scale": "1"
	},
	{
		"name": "good object 7",
		"scale": "1"
	},
	{
		"name": "good object 8",
		"scale": "1"
	},
	{
		"name": "good object 9",
		"scale": "1"
	},
	{
		"name": "good object 10",
		"scale": "1"
	},
	{
		"name": "bad object 1",
		"scale": "1"
	},
	{
		"name": "bad object 2",
		"scale": "1"
	},
	{
		"name": "bad object 3",
		"scale": "1"
	},
	{
		"name": "bad object 4",
		"scale": "1"
	},
	{
		"name": "bad object 5",
		"scale": "1"
	},
	{
		"name": "bad object 6",
		"scale": "1"
	},
	{
		"name": "bad object 7",
		"scale": "1"
	},
	{
		"name": "bad object 8",
		"scale": "1"
	},
	{
		"name": "bad object 9",
		"scale": "1"
	},
	{
		"name": "bad object 10",
		"scale": "1"
	},
	{
		"name": "bad object 11",
		"scale": "1"
	},
	{
		"name": "bad object 12",
		"scale": "1"
	},
	{
		"name": "bad object 13",
		"scale": "1"
	},
	{
		"name": "bad object 14",
		"scale": "1"
	},
	{
		"name": "bad object 15",
		"scale": "1"
	},
	{
		"name": "bad object 16",
		"scale": "1"
	},
	{
		"name": "bad object 17",
		"scale": "1"
	},
	{
		"name": "bad object 18",
		"scale": "1"
	},
	{
		"name": "bad object 19",
		"scale": "1"
	},
	{
		"name": "bad object 20",
		"scale": "1"
	},
	{
		"name": "bad object 21",
		"scale": "1"
	},
	{
		"name": "bad object 22",
		"scale": "1"
	}
]