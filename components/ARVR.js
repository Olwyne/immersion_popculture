import * as THREE from "../build/three.module.js";

//IMPORT ASSETS
import { ARButton } from "../jsm/webxr/ARButton.js";
import { VRButton } from "../jsm/webxr/VRButton.js";

import { RGBELoader } from "../jsm/loaders/RGBELoader.js";
import { GLTFLoader } from "../jsm/loaders/GLTFLoader.js";

var touchDown, touchX, touchY, deltaX, deltaY;
var isAR

export function createButton(
  current_object,
  scene,
  renderer
) {

  $("#ARButton").click(function () {
    current_object.visible = false;
    isAR = true;
  });

  $("#VRButton").click(function () {
  });

  $("#place-button").click(function () {
    arPlace(scene);
  });

  //VR SETUP
  document.body.appendChild(VRButton.createButton(renderer));

  //AR SETUP

  let options = {
    requiredFeatures: ["hit-test"],
    optionalFeatures: ["dom-overlay"],
  };

  options.domOverlay = { root: document.getElementById("content") };

  document.body.appendChild(ARButton.createButton(renderer, options));
  initListeners(renderer)
}

function arPlace(scene) {
  const reticle = new THREE.Mesh(
    new THREE.RingBufferGeometry(0.15, 0.2, 32).rotateX(-Math.PI / 2),
    new THREE.MeshBasicMaterial()
  );
  reticle.matrixAutoUpdate = false;
  reticle.visible = false;
  scene.add(reticle);
  if (reticle.visible) {
    current_object.position.setFromMatrixPosition(reticle.matrix);
    current_object.visible = true;
  }
}

export function loadModel(render, current_object, controls, scene) {
	new RGBELoader()
	.setDataType(THREE.UnsignedByteType)
	.setPath("textures/")
	.load("photo_studio_01_1k.hdr", function (texture) {
		// envmap = pmremGenerator.fromEquirectangular(texture).texture;

		// scene.environment = envmap;
		// texture.dispose();
		// pmremGenerator.dispose();
		render();

		var loader = new GLTFLoader().setPath("3d/");
		loader.load("1.glb", function (glb) {
		current_object = glb.scene;
		//scene.add(current_object);

		arPlace(scene);

		var box = new THREE.Box3();
		box.setFromObject(current_object);
		box.center(controls.target);

		controls.update();
		render();
		});
	});
}


function rotateObject() {
    if (current_object && reticle.visible) {
      current_object.rotation.y += deltaX / 100;
    }
  }

function initListeners(renderer) {
    renderer.domElement.addEventListener(
        "touchstart",
        function (e) {
          e.preventDefault();
          touchDown = true;
          touchX = e.touches[0].pageX;
          touchY = e.touches[0].pageY;
        },
        false
      );
    
      renderer.domElement.addEventListener(
        "touchend",
        function (e) {
          e.preventDefault();
          touchDown = false;
        },
        false
      );
    
      renderer.domElement.addEventListener(
        "touchmove",
        function (e) {
          e.preventDefault();
    
          if (!touchDown) {
            return;
          }
    
          deltaX = e.touches[0].pageX - touchX;
          deltaY = e.touches[0].pageY - touchY;
          touchX = e.touches[0].pageX;
          touchY = e.touches[0].pageY;
    
          rotateObject();
        },
        false
      );
}

export function render(timestamp, frame) {
    if (frame && isAR) {
      var referenceSpace = renderer.xr.getReferenceSpace();
      var session = renderer.xr.getSession();
  
      if (hitTestSourceRequested === false) {
        session.requestReferenceSpace("viewer").then(function (referenceSpace) {
          session
            .requestHitTestSource({ space: referenceSpace })
            .then(function (source) {
              hitTestSource = source;
            });
        });
  
        session.addEventListener("end", function () {
          hitTestSourceRequested = false;
          hitTestSource = null;
  
          isAR = false;
  
          reticle.visible = false;
  
          var box = new THREE.Box3();
          box.setFromObject(current_object);
          box.center(controls.target);
  
          document.getElementById("place-button").style.display = "none";
        });
  
        hitTestSourceRequested = true;
      }
  
      if (hitTestSource) {
        var hitTestResults = frame.getHitTestResults(hitTestSource);
  
        if (hitTestResults.length) {
          var hit = hitTestResults[0];
  
          document.getElementById("place-button").style.display = "block";
  
          reticle.visible = true;
          reticle.matrix.fromArray(hit.getPose(referenceSpace).transform.matrix);
        } else {
          reticle.visible = false;
  
          document.getElementById("place-button").style.display = "none";
        }
      }
    }
  }