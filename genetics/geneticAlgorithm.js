/**
 * Le génome doit informer sur : 22 bits
 * - capacité à avoir des enfants : (4 bits - 16 valeurs possibles)
 * // - choix des objets (5 bits - 32 valeurs possibles)
 * - scale x des objets (2 bits - 4 valeurs possibles)
 * - scale y des objets (2 bits - 4 valeurs possibles)
 * - scale z des objets (2 bits - 4 valeurs possibles)
 * - rotation x des objets (2 bits - 4 valeurs possibles)
 * - rotation y des objets (2 bits - 4 valeurs possibles)
 * - rotation z des objets (2 bits - 4 valeurs possibles)
 * - speed of rotation (2 bits - 4 valeurs possibles)
 * - glitches (1 bits - 2 valeurs possibles)
 * - distance from center (3 bits - 8 valeurs possibles)
 */
var genomeLength = 22

export function randomBit() {
	return Math.round(Math.random())
}

export function generateInitialPopulation(maxIndividuals = 50) {
	const numOfIndividuals = Math.round(Math.random() * (maxIndividuals - 2)) + 2
	let familly = []
	do {
		let genome = ''
		do {
			genome += (Math.random() < 0.1 )?'1':'0'
		} while (genome.length < genomeLength)

		familly.push(genome)
	} while (familly.length < numOfIndividuals)

	return familly
}

export function fitness(genome) {
	return genome.split('').reduce((acc, currBit) => acc+=parseInt(currBit), 0)
}

export function sortGenomes(gen) {
	return gen.sort((a, b) => {
		return fitness(b) - fitness(a)
	})
}

export function generateFecondationMask(genomeLength) {
	let mask = []
	while (mask.length < genomeLength) {
		mask.push(randomBit())
	}
	return mask
}

export function getGenomeFromParents(parent_1, parent_2, merge = false) {
	let newGenome = ''
	
	const newMask = generateFecondationMask(parent_1.length)
	const inversionIndex1 = Math.floor(Math.random() * parent_1.length)
	const inversionIndex2 = Math.floor(Math.random() * parent_1.length)

	for (let i=0; i<parent_1.length; i++) {
		let newBit
		//if (parent_1[i] === '0' && parent_2[i] === '0')
			//newBit = (Math.random() < 0.3) ? '1' : '0'
		//if (parent_1[i] === '1' || parent_2[i] === '0')
			//newBit = (Math.random()<0.7)?'1':'0'
		//else
			//newBit = '1'
		if (merge) {
			if (parent_1[i] === '1' || parent_2[i] === '1')
				newBit = '1'
			else
				newBit = '0'
		}
		else {
			newBit = (newMask[i] === 1) ? parent_1[i] : parent_2[i]
			if (i === inversionIndex1) {
				if (randomBit() === 1 && newBit !== '1')
					newBit = '1'
				else
					newBit = (newBit === '1') ? '0' : '1'
			}
			if (i === inversionIndex2)
				newBit = '1'
		}
		newGenome += newBit
	}

	return newGenome
}

function getKidsNumberFromParents(parent_1, parent_2) {
	return fitness(getGenomeFromParents(parent_1, parent_2, true))/parent_1.length * 12
}

function getSurvivors(generation) {
	let survivors = []
	for (let i = 0; i<generation.length; i++) {
		//const survivalChance = 1 - ((i+1)/generation.length * 0.9) // + ((i <= 10) ? 0.5 : 0) + ((generation.length <=2) ? 2 : 0)
		//const survivalChance = 1 - (((i+1)/generation.length)*((0.95-(0.1*generation.length - 0.95)/generation.length - 1)) + (0.1*generation.length - 0.95)/(generation.length - 1)) // + ((i <= 10) ? 0.5 : 0) + ((generation.length <=2) ? 2 : 0)
		//const survivalChance = 1 - ((((i)+1)*2)/generation.length) * (0.95 - 0.1)
		const survivalChance = (fitness(generation[i])/2)/generation[i].length
		//console.log(`[${i}] survivalChance ${survivalChance}`)
		if (Math.random() <= survivalChance) {
			//console.log(`parent kept ${i+1}/${generation.length}`)
			survivors.push(generation[i])
		}
		//if (i < 10)
	}
	//console.log(`${survivors.length} survivors from ${generation.length},`, survivors.map(sur => fitness(sur)))
	while (survivors.length <2) {
		survivors.push(generation[survivors.length])
	}
	return survivors
}

export function generateNewGeneration(previousGeneration) {
	//const parentsToKeep = (previousGeneration.length < 2) ? previousGeneration.length : 2 + Math.trunc(previousGeneration.length * 0.33)
	//const genomePerParent = Math.round(Math.random() * 2  + 0.5)
	let newGeneration = []
	const survivors = getSurvivors(previousGeneration)
	//console.log('parentsToKeep', survivors.length, 'from', previousGeneration.length, 'genomes')
	for (let i=0; i < survivors.length; i+=2) {
		const parent_1 = Math.floor(Math.random() * survivors.length)

		// Build other parent array (one parent cannot fecondate itself)
		let otherParents = []
		for (let j=0; j<survivors.length; j++) {
			if (j!==parent_1)
				otherParents.push(j)
		}
		const parent_2 = otherParents[Math.floor(Math.random() * otherParents.length)]
		//console.log('parent quality', fitness(survivors[parent_1]), fitness(survivors[parent_2]))
		for (let i=0; i < getKidsNumberFromParents(survivors[parent_1], survivors[parent_2]); i++) {
			newGeneration.push(getGenomeFromParents(survivors[parent_1], survivors[parent_2]))
		}
	}
	return newGeneration
}

export function getGenerationQuality(generation, from = 0, to) {
	return generation.map(genome => fitness(genome.slice(from, to)))
		.reduce((totalFitness, currentGenomeFitness) => totalFitness+=currentGenomeFitness, 0) / generation.length
}

export function getNumFromSequence(bitSequence) {
	return bitSequence.split('').reduce((acc, currBit, index) => acc+=parseInt(currBit) * Math.pow(2, index), 0)
}

/**
 * Gives reference to object
 */
export function getObjectIdFromSequence(bitSequence) {
	return getNumFromSequence(bitSequence)
}

/**
 * We get the scale shift
 */
export function getScaleFromSequence(bitSequence) {
	const num = getNumFromSequence(bitSequence)
	switch (num) {
		case 0:
			return 0.4
		case 1:
			return 0.6
		case 2:
			return 0.8
		default:
			return 1;
	}
}

export function getRotationFromSequence(bitSequence) {
	const num = getNumFromSequence(bitSequence)
	switch (num) {
		case 0:
			return -0.1
		case 1:
			return 0.1
		case 2:
			return 0.2
		default:
			return 1;
	}
}

export function getRotationSpeedFromSequence(bitSequence) {
	const num = getNumFromSequence(bitSequence)
	switch (num) {
		case 0:
			return 30
		case 1:
			return 15
		case 2:
			return 5
		default:
			return 0;
	}
}

function getFecondationAbilityFromSequence(bitSequence) {
	return getNumFromSequence(bitSequence) / 4
}

export function getScaleSpeedFromSequence(bitSequence) {
	const num = getNumFromSequence(bitSequence)
	switch (num) {
		case 0:
			return 0.4
		case 1:
			return 0.6
		case 2:
			return 0.8
		default:
			return 1;
	}
}

export function getGlitchesFromSequence(bitSequence) {
	return (getNumFromSequence(bitSequence) > 1) ? false : true
}

export function getDistanceFromSequence(bitSequence) {
	return Math.random() * getNumFromSequence(bitSequence) + 5
}

function getTextureFromSequence(bitSequence) {
	return Math.floor(getNumFromSequence(bitSequence) / (Math.pow(2, bitSequence.length)) * 37)
}

export function getDataFromGenome(genome) {
  return {
    //objectId: getObjectIdFromSequence(genome.slice(0, 5)),
    scale: {
      x: getScaleFromSequence(genome.slice(3, 5)),
      y: getScaleFromSequence(genome.slice(5, 7)),
      z: getScaleFromSequence(genome.slice(7, 9)),
    },
    //rotation: {
      //x: getRotationFromSequence(genome.slice(9, 11)),
      //y: getRotationFromSequence(genome.slice(11, 13)),
      //z: getRotationFromSequence(genome.slice(13, 15)),
      //speed: getRotationSpeedFromSequence(genome.slice(15, 17)),
    //},
	  texture: getTextureFromSequence(genome.slice(9, 16)),
	  opacity: getScaleSpeedFromSequence(genome.slice(16, 18)),
    glitches: getGlitchesFromSequence(genome.slice(17, 20)),
    distance: getDistanceFromSequence(genome.slice(19, 22)),
  };
}


// Initial generation
//let generation = generateInitialPopulation(genomeLength)
//console.log(getGenerationQuality(generation))

// Successive generations
//for (let i=0; i<35; i++) {
//	generation = sortGenomes(generation)
//	generation = generateNewGeneration(generation)
//	const generationQuality = getGenerationQuality(generation)
//	//console.log(generationQuality, 'sur', genomeLength)
//}f
