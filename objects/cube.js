
import * as THREE from "../build/three.module.js";

export default class Cube extends THREE.Mesh {

  constructor(x,y,z,h,l,p,c,w) {
    const geometry = new THREE.BoxGeometry( l,h,p,50,50);
    
    const material = new THREE.MeshPhysicalMaterial( { color: c, wireframe: w} );
    super(geometry, material);
    this.material.side = THREE.DoubleSide;

    this.position.x = x;
    this.position.y = y;
    this.position.z = z;
  }

  update() {
  }

}