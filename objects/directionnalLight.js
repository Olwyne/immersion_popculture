
import * as THREE from "../build/three.module.js";

export default function directionnalLight() {
  const directionalLight = new THREE.DirectionalLight(0xdddddd, 1);
  directionalLight.position.set(1, 1, 1).normalize();
  return directionalLight
}