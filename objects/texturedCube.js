
import * as THREE from "../build/three.module.js";

export default class TexturedCube extends THREE.Mesh {

  constructor(x,y,z,h,l,p,c,w) {

    const geometry = new THREE.BoxGeometry( l,h,p,50,50);

    const loader = new THREE.CubeTextureLoader();
    loader.setPath( 'textures/' );
    
    const textureCube = loader.load( [
      'texture.png', 'texture.png',
      'texture.png', 'texture.png',
      'texture.png', 'texture.png'
    ] );
    
    const material = new THREE.MeshBasicMaterial( { color: 0xffffff, envMap: textureCube } );

  super(geometry, material);
  this.position.x = x;
  this.position.y = y;
  this.position.z = z;
    
  }

  update() {
  }

}